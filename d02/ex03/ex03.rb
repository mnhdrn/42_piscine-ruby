#!/usr/bin/env ruby -w

#==============================================================================#
#------------------------------------------------------------------------------#
class Elem
	attr_reader :tag, :content, :tag_type, :opt

	def initialize(tag="p", content=[], tag_type="double", opt={})
		@tag = tag
		@content = content
		if tag_type.eql?("double") || tag_type.eql?("simple")
			@tag_type = tag_type
		else
			@tag_type = "double"
		end
		@opt = opt
	end

	def add_content(*new_content)
		new_content.each { |e|
			@content << e
		}
	end

	def to_s
		finish = false
		# OPEN TAG
		s = '<' + self.tag
		# SET OPT
		@opt.each{ |key, value|
			s = s + " #{key}='#{value}'"
		}
		# CLOSE THE FIRST
		if @tag_type.eql?("double")
			if @tag.eql?("html") || @tag.eql?("head") || @tag.eql?("body")
				s = s + ">\n"
			else
				s = s + ">"
			end
			finish = true
		end
		# PUT CONTENT IF NO SIMPLE
		if @content.class == [].class
			@content.each{|e| s = s + e.to_s + "\n"}
		else
			s += @content.to_s
		end
		# CLOSE TAG
		if finish == true
			s = s + "</#{@tag}>"
		else
			s = s + " />"
		end
	end
end

#==============================================================================#
#------------------------------------------------------------------------------#
class Text
	attr_reader :string
	def initialize(str)
		@string = str
	end
	def to_s
		@string
	end
end

if $PROGRAM_NAME == __FILE__
	a = Elem.new("body", [], "cheh")
	b = Elem.new("h1", Text.new("it's working"), "")
	c = Elem.new("img", "", "simple", {"src":"https://google.com"})
	d = Elem.new("p", Text.new("I'm a simple element"))
	a.add_content(b, c, d)
	puts a.to_s
end

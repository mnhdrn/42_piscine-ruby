#!/usr/bin/env ruby -w

class Html
	attr_reader :page_name

	def initialize(filename)
		@page_name = filename
		head
	end

	def head
		if File.exist?("#{@page_name}.html")
			raise "A file named #{@page_name} already exist!"
		else
			File.open("#{@page_name}.html", 'w') {
				|file| file.write("<!DOCTYPE html>\n"\
								  "<html>\n"\
								  "<head>\n"\
								  "<meta charset=\"UTF-8\">\n"\
								  "<title>#{@page_name}</title>\n"\
								  "</head>\n"\
								  "<body>\n")
			}
		end
	end

	def dump(str)
		if !seek("<body>")
			raise "There is no body tag in #{@page_name}"
		elsif seek("</body>")
			raise "Body has already been closed in #{@page_name}.html"
		end
		File.open("#{@page_name}.html", 'a') {
			|file| file.write("  <p>#{str}</p>\n")
		}
	end

	def finish
		if seek("</body>")
			raise "#{@page_name} has already been closed."
		else
			File.open("#{@page_name}.html", 'a') {
				|file| file.write("</body>\n")
			}
		end
	end

	def seek(str)
		File.readlines("#{@page_name}.html").each{ |line|
			if line.include?("#{str}")
				return true
			end
		}
		return false
	end
end

if $PROGRAM_NAME == __FILE__
	a = Html.new("test")
	10.times{|x| a.dump("titi_number#{x}")}
	a.finish
	b = Html.new("test2")
	10.times{|x| b.dump("titi_number#{x}")}
	b.finish
	b.dump "AGAIN"
	b.finish
end

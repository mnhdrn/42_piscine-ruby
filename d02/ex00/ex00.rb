#!/usr/bin/env ruby -w

class Html
	attr_reader :page_name

	def initialize(filename)
		@page_name = filename
		head
	end

	def head
		File.open("#{@page_name}.html", 'w') {
			|file| file.write("<!DOCTYPE html>\n"\
							  "<html>\n"\
							  "<head>\n"\
							  "<meta charset=\"UTF-8\">\n"\
							  "<title>#{@page_name}</title>\n"\
							  "</head>\n"\
							  "<body>\n")
		}
	end

	def dump(str)
		File.open("#{@page_name}.html", 'a') {
			|file| file.write("  <p>#{str}</p>\n")
		}
	end

	def finish
		File.open("#{@page_name}.html", 'a') {
			|file| file.write("</body>\n")
		}
	end
end

if $PROGRAM_NAME == __FILE__
	a = Html.new("mon_test_en_plume")
	10.times{|x| a.dump("titi_number#{x}")}
	a.finish
end

#!/usr/bin/env ruby -w

#==============================================================================#
#------------------------------------------------------------------------------#
class Html
	attr_reader :page_name

	def initialize(filename)
		@page_name = filename
		head
	end

	def head
		if File.exist?("#{@page_name}.html")
			begin
				raise excpt = Dup_file.new(@page_name)
			rescue => excpt
				excpt.show_state
				@page_name = excpt.correct
				excpt.explain
			end
		end
		File.open("#{@page_name}.html", 'w') {
			|file| file.write("<!DOCTYPE html>\n"\
							  "<html>\n"\
							  "<head>\n"\
							  "<meta charset=\"UTF-8\">\n"\
							  "<title>#{@page_name}</title>\n"\
							  "</head>\n"\
							  "<body>\n")
		}
	end

	def dump(str)
		if !seek("<body>")
			raise "There is no body tag in #{@page_name}"
		elsif seek("</body>")
			begin 
				raise excpt = Body_closed.new(@page_name)
			rescue => excpt
				excpt.show_state
				excpt.correct(str)
				excpt.explain
			File.open("#{@page_name}.html", 'a') { |file|
				file.write("  <p>#{str}</p>\n")
				file.write("</body>\n")
			}
			end
		else
			File.open("#{@page_name}.html", 'a') {
				|file| file.write("  <p>#{str}</p>\n")
			}
		end
	end

	def finish
		if seek("</body>")
			raise "#{@page_name} has already been closed."
		else
			File.open("#{@page_name}.html", 'a') {
				|file| file.write("</body>\n")
			}
		end
	end

	def seek(str)
		File.readlines("#{@page_name}.html").each{ |line|
			if line.include?("#{str}")
				return true
			end
		}
		return false
	end
end

#==============================================================================#
#------------------------------------------------------------------------------#
class Dup_file < StandardError
	attr_reader :page_name

	def initialize(filename)
		@page_name = filename
	end

	def show_state
		path = File.expand_path("#{@page_name}.html")
		puts "A file named #{@page_name} was already there : #{path}"
	end

	def correct
		@add_new = "#{@page_name}.new"
		if File.file?("#{@add_new}.html")
			begin
				raise err = Dup_file.new(@add_new)
			rescue => err
				@add_new = err.correct
			end
		end
		File.open("#{@add_new}.html", 'a')
		@add_new
	end

	def explain
		path = File.expand_path("#{@add_new}.html")
		puts "Appended .new in order to create requested file: #{path}"
	end
end

#==============================================================================#
#------------------------------------------------------------------------------#
class Body_closed < StandardError
	attr_reader :page_name

	def initialize(filename)
		@page_name = filename
	end

	def show_state
		puts "In #{@page_name}.html body was closed."
	end

	def correct(str)
		data = []
		File.open("#{@page_name}.html", "r") { |file|
			file.each_with_index { |line, nb_line|
				if !line.include? "</body>"
					data << line
				else
					@nb_line = nb_line + 1
				end
			}
		}
		File.open("#{@page_name}.html", "w") { |file|
			data.each { |line| 
				file.write(line)
			}
		}
	end

	def explain
		puts "\t> ln :#{@nb_line} </body> : "\
			"text has been inserted and tag moved at the end of it."
	end
end

if $PROGRAM_NAME == __FILE__
	a = Html.new("test")
	10.times{|x| a.dump("nb - #{x}")}
	a.finish
	a.dump("Crash")
end
